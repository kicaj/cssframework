$(document).ready(function() {
	/**
	 * Disable anchor tag with button-disabled class or attribute disabled
	 */
	$('button[disabled], input[disabled], button.button-disabled, input.button-disabled, a.button-disabled, a.button[disabled]').on('click', function(event) {
		event.stopPropagation();
		event.preventDefault();

		return false;
	});

	/**
	 * Normalize input type file by wrapping class button-file
	 */
	$('input[type=file]').each(function () {
		$(this).wrap('<div class="button-file ' + $(this).attr('class') + '" />').parent('.button-file').prepend('<button type="button" tabindex="-1">' + $(this).attr('placeholder') + '</button>');
	});

	$('.button-file').on('click', 'button', function(event) {
		event.stopPropagation();
		event.preventDefault();

		if ($(this).not(':disabled')) {
			$(this).parent().find('input[type="file"]').prop('disabled', false);
		}

		$(this).parent().find('input[type="file"]').trigger('click');

		return false;
	}).each(function() {
		if ($(this).find('input[type="file"]').is(':disabled')) {
			$(this).find('button').prop('disabled', true);
		}
	});

	/**
	 * Duplicate last input type file if has class button-copy
	 */
	$('.button-file').on('change', 'input[type=file]', function() {
		if ($(this).hasClass('button-copy') && $(this).parents('.button-file').is(':last-child')) {
			var clone = $(this).parents('.button-file').clone(true);

			clone.find('input[type=file]').attr({
				'name': clone.find('input[type=file]').attr('name').replace(/\d/, function(i) {
					return parseInt(i) + 1;
				}),
				'id': clone.find('input[type=file]').attr('id').replace(/\d/, function(i) {
					return parseInt(i) + 1;
				})
			}).val('');

			clone.addClass('offset-3');

			$(this).parents('fieldset').append(clone);
		}

		$(this).parent().children('ins').remove();
		$(this).parent().children('button').after('<ins>' + $(this).val().replace(/^.*[\\\/]/, '') + '</ins>');
	}).on('focusin', 'input[type=file]', function() {
		$(this).parent().children('button').addClass('button-hover');
	}).on('focusout', 'input[type=file]', function() {
		$(this).parent().children('button').removeClass('button-hover');
	});

	/**
	 * Normalize select by wrapping class button-select
	 */
	$('select').each(function() {
		$(this).wrap('<div class="button-select">');
		$(this).parent('.button-select').append('<button type="button" tabindex="-1"></button>');
	});

	$('.button-select').on('click', 'button', function(event) {
		event.stopPropagation();
		event.preventDefault();

		return false;
	});

	$('.button-select').on('mousedown', 'select', function() {
		$(this).children('button').addClass('button-active');
	}).on('change', 'select', function() {
		$(this).parent().children('button').text($(this).children('option:selected').text().trim());

		$(this).children('button').removeClass('button-active');
	}).on('click', 'select', function() {
		$(this).parent().children('button').text($(this).children('option:selected').text().trim());
	}).on('focusin', 'select', function() {
		$(this).parent().children('button').addClass('button-hover');
	}).on('focusout', 'select', function() {
		$(this).parent().children('button').removeClass('button-hover');
	}).on('mouseover', function() {
		$(this).children('button').addClass('button-hover');
	}).on('mouseleave', function() {
		$(this).children('button').removeClass('button-hover');
	}).each(function() {
		$(this).children('button').text($(this).children('select').children('option:selected').text().trim());

		$(this).closest('.button-select').addClass($(this).children('select').attr('class'));
		$(this).children('select').removeAttr('class');
		$(this).children('select, button').css('width', '100%');
	});
});